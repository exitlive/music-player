package live.exit.musicplayer

import android.support.v4.media.session.MediaSessionCompat
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodChannel

class MusicPlayerPlugin : FlutterPlugin, ActivityAware {
    var channel: MethodChannel? = null

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(binding.binaryMessenger, "exit.live/music_player")
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        val activity = binding.activity
        val session = MediaSessionCompat(activity.applicationContext, "MediaSession")
        val player = MusicPlayer(channel!!, session, activity)
        val plugin = MusicPlayerPluginImpl(player, activity.applicationContext, activity, session)
        activity.application.registerActivityLifecycleCallbacks(plugin)
        channel!!.setMethodCallHandler(plugin)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        TODO("Not yet implemented")
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onDetachedFromActivity() {
        TODO("Not yet implemented")
    }
}